// Camera Manipulation through keyboard keys
// This programs play with gllookat arguments 
// This programs implements orbit, pan, zoom operation

//Logic -- In orbit change the camera position and upvectors -- compute the transformed points
//         The camera target is the origine of the MV matrix which hold camera transformation.
//         Camera is always at the z with some distance of from the origine of mv matrix
//         Orbit is done along x,z -- through keyboard keys (L/R/U/D arrow)

//Logic -- Zoom In/Out on the keys +/-
//         
//         
//         

// Note : No changes mode in the grojection mode it is gluprespective
// Here we are playing with camera we will work on projection in next exercise

#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define _USE_MATH_DEFINES
#include "math.h"

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable decleration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLUquadric *quadric = NULL;

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

float zmoveFactor = 100;

class Point3D
{
	float x, y, z;

	Point3D()
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}
};

class Camera
{
public:
	Point3D cameraPos;
	Point3D cameratarget;
	Point3D upVector;
};

// All the below points are calculated in world coordinate because 
// we are creating our own matrix for tansformation and calculating the transform points.
// To get the point of object space where camera is always along the z-axis (on orbit axis rotates camera doesn't)

float viewingLength = 5.0f;
float position[3]; //one point 0,0,0 and other at 0,0,5 the object coordinate mv matric is at origin
float upVector[3];
float target[3];



GLfloat CameraMatrix[16];

float ComputeVectorLength(float x, float y, float z)
{
	return sqrt((x*x) + (y*y) + (z*z));
}

void ComputeNormalizedVector(float *x, float *y, float *z)
{
	float length = ComputeVectorLength(*x, *y, *z);
	*x = *x / length;
	*y = *y / length;
	*z = *z / length;
}

bool MultMatrix(float *matA, float *matB, int rA, int cA, int rB, int cB, float * result)
{
	if (cA != rB)
		return false; //can't muiltply

	float temp[16]; int rC = rA;  int cC = cB;

	for (int i = 0; i < rA; i++)
	{
		for (int j = 0; j < cB; j++)
		{
			float sum = 0.0;
			for (int k = 0; k < rB; k++)
				sum = sum + matA[i * cA + k] * matB[k * cB + j];
			temp[i * cC + j] = sum;
		}
	}

	int size = rC*cC;
	for (int i = 0; i < size; i++)
	{
		result[i] = temp[i];
	}

	return true;
}

bool ComputeTransformedPoints(float *transformMatrix, float* points)
{
	float transposeTransform[16];
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			*(transposeTransform + (i * 4 + j)) = *(transformMatrix + (j * 4 + i));
		}
	}

	MultMatrix(transposeTransform, points, 4, 4, 4, 1, points);
	return true;
}

bool RotateY(float angle, float* points)
{
	GLfloat angleRadian = angle *(M_PI / 180);

	GLfloat RotateMatrixY[16];
	//This function takes translation unit and create new CTM
	RotateMatrixY[0] = cos(angleRadian);
	RotateMatrixY[1] = 0.0f;
	RotateMatrixY[2] = sin(angleRadian);
	RotateMatrixY[3] = 0.0f;

	RotateMatrixY[4] = 0.0f;
	RotateMatrixY[5] = 1.0f;
	RotateMatrixY[6] = 0.0f;
	RotateMatrixY[7] = 0.0f;

	RotateMatrixY[8] = -sin(angleRadian);
	RotateMatrixY[9] = 0.0f;
	RotateMatrixY[10] = cos(angleRadian);
	RotateMatrixY[11] = 0.0f;

	RotateMatrixY[12] = 0.0f;
	RotateMatrixY[13] = 0.0f;
	RotateMatrixY[14] = 0.0f;
	RotateMatrixY[15] = 1.0f;

	MultMatrix(RotateMatrixY, CameraMatrix, 4, 4, 4, 4, CameraMatrix);

	ComputeTransformedPoints(CameraMatrix, points);

	return true;
}


bool RotateX(float angle, float* points)
{
	GLfloat angleRadian = angle *(M_PI / 180);

	GLfloat RotateMatrixX[16];
	//This function takes translation unit and create new CTM
	RotateMatrixX[0] = 1.0f;
	RotateMatrixX[1] = 0.0f;
	RotateMatrixX[2] = 0.0f;
	RotateMatrixX[3] = 0.0f;

	RotateMatrixX[4] = 0.0f;
	RotateMatrixX[5] = cos(angleRadian);
	RotateMatrixX[6] = sin(angleRadian);
	RotateMatrixX[7] = 0.0f;

	RotateMatrixX[8] = 0.0f;
	RotateMatrixX[9] = -sin(angleRadian);
	RotateMatrixX[10] = cos(angleRadian);
	RotateMatrixX[11] = 0.0f;

	RotateMatrixX[12] = 0.0f;
	RotateMatrixX[13] = 0.0f;
	RotateMatrixX[14] = 0.0f;
	RotateMatrixX[15] = 1.0f;

	MultMatrix(RotateMatrixX, CameraMatrix, 4, 4, 4, 4, CameraMatrix);

	ComputeTransformedPoints(CameraMatrix, points);

	return true;
}

void DrawCube()
{
	glBegin(GL_QUADS);
	{
		//Front Face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f); //top Left front
		glVertex3f(-1.0f, -1.0f, 1.0f); //bottom Left front
		glVertex3f(1.0f, -1.0f, 1.0f); //bottom Right front
		glVertex3f(1.0f, 1.0f, 1.0f); //top Right front

									  //Back Face -- make Z negative of front face
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f);

		//Left Face
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f); //top Left back
		glVertex3f(-1.0f, -1.0f, -1.0f); //bottom Left back
		glVertex3f(-1.0f, -1.0f, 1.0f); //bottom Left front
		glVertex3f(-1.0f, 1.0f, 1.0f); //top Left front

									   //Right Face
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);

		//Top Face
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f); //top Right front
		glVertex3f(1.0f, 1.0f, -1.0f); //top Right back
		glVertex3f(-1.0f, 1.0f, -1.0f); //top Left back
		glVertex3f(-1.0f, 1.0f, 1.0f); //top Left front

									   //Bottom Face
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

	}glEnd();
}

void onFrontView()
{
	//Make the camera matrix identity
}

void onBackView()
{
	//Make the camera matrix identity
	//Rotate the camera matrix and calculate transformed camera position and up vector
}

void onLeftView()
{
	//Make the camera matrix identity
	//Rotate the camera matrix and calculate transformed camera position and up vector
}

void onRightView()
{
	//Make the camera matrix identity
	//Rotate the camera matrix and calculate transformed camera position and up vector
}

void onTopView()
{
	//Rotate the camera matrix and calculate transformed camera position and up vector
}

void onBottomView()
{
	//Make the camera matrix identity
	//Rotate the camera matrix and calculate transformed camera position and up vector
}

void onIsometricView()
{
	//Make the camera matrix identity
	//Rotate the camera matrix and calculate transformed camera position and up vector
}

void onRightKeyPressed()
{
	float temp[3] = { 0.0f, 0.0f, 0.0f };
	temp[2] = viewingLength;
	RotateY(1, temp);
	position[0] = temp[0];
	position[1] = temp[1];
	position[2] = temp[2];
}

void onLeftKeyPressed()
{
	float temp[3] = { 0.0f, 0.0f, 0.0f };
	temp[2] = viewingLength;
	RotateY(-1, temp);
	position[0] = temp[0];
	position[1] = temp[1];
	position[2] = temp[2];
}

void onUpKeyPressed()
{
	float temp[3] = {0.0f, 0.0f, 0.0};
	temp[2] = viewingLength;
	RotateX(1, temp);
	position[0] = temp[0];
	position[1] = temp[1];
	position[2] = temp[2];

	float upvectemp[3] = { 0.0f, 1.0f, 0.0f };
	temp[2] = viewingLength;
	ComputeTransformedPoints(CameraMatrix, upvectemp);
	upVector[0] = upvectemp[0];
	upVector[1] = upvectemp[1];
	upVector[2] = upvectemp[2];
}

void onDownKeyPressed()
{
	float temp[3] = { 0.0f, 0.0f, 0.0f };
	temp[2] = viewingLength;
	RotateX(-1, temp);
	position[0] = temp[0];
	position[1] = temp[1];
	position[2] = temp[2];

	float upvectemp[3] = { 0.0f, 1.0f, 0.0f };
	ComputeTransformedPoints(CameraMatrix, upvectemp);
	upVector[0] = upvectemp[0];
	upVector[1] = upvectemp[1];
	upVector[2] = upvectemp[2];
}

void onNumKeyboardPlus()
{
	// Zoom in translate camera position along view vector - it is z here
	// The z is not world systems z it is object system z. Position and target both are in world.

	//1. Convert position and target from world to object by reverse trasformation.
	//   Viewing vector will automatically become z.
	//2. Calculate viewing vector in world translate along vewing vector

	//Note: You can zoom till position is not equal to target 

	float viewingvector[3];
	viewingvector[0] = target[0] - position[0];
	viewingvector[1] = target[1] - position[1];
	viewingvector[2] = target[2] - position[2];
	
	viewingLength = ComputeVectorLength(viewingvector[0], viewingvector[1], viewingvector[2]);

	float offset = viewingLength * 0.01; //reduce viewing distance by 10 percent
	viewingLength *= 0.90;
    
	ComputeNormalizedVector(&viewingvector[0], &viewingvector[1], &viewingvector[2]);
	
	viewingvector[0] = offset*viewingvector[0];
	viewingvector[1] = offset*viewingvector[1];
	viewingvector[2] = offset*viewingvector[2];

	position[0] = position[0] + viewingvector[0];
	position[1] = position[1] + viewingvector[1];
	position[2] = position[2] + viewingvector[2];

}

void onNumKeyboardMinus()
{
	//Zoom out in translate along +z
	// Zoom in translate camera position along view vector - it is z here

	//1. Convert position and target from world to object by reverse trasformation.
	//   Viewing vector will automatically become z.
	//2. Calculate viewing vector in world translate along vewing vector

	// Note: W can zoom out till any point no limit
	// we need to set some limit

	float viewingvector[3];
	viewingvector[0] = target[0] - position[0];
	viewingvector[1] = target[1] - position[1];
	viewingvector[2] = target[2] - position[2];

	// We have to zoom out so we need to come out of the viewing direction
	// so projecting camera position in reverse of view.

	viewingvector[0] = -1*viewingvector[0];
	viewingvector[1] = -1*viewingvector[1];
	viewingvector[2] = -1*viewingvector[2];

	viewingLength = ComputeVectorLength(viewingvector[0], viewingvector[1], viewingvector[2]);

	float offset = viewingLength * 0.01; //reduce viewing distance by 10 percent
	viewingLength += offset;

	ComputeNormalizedVector(&viewingvector[0], &viewingvector[1], &viewingvector[2]);

	viewingvector[0] = offset*viewingvector[0];
	viewingvector[1] = offset*viewingvector[1];
	viewingvector[2] = offset*viewingvector[2];

	position[0] = position[0] + viewingvector[0];
	position[1] = position[1] + viewingvector[1];
	position[2] = position[2] + viewingvector[2];

}

void onNumKeyboardMUL()
{
   //Pan right
   //Pan means keep the position as it is change the target and up vector
   //In one sense it is opposite tp the orbit.

   

}

void onNumKeybardDiv()
{
   //Pan left

}

//Main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//Variable decleration

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//Code
	//Initilizing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Register Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline using Native Windowing "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		600,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}

//Wnd Proc()

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lparam)
{
	//Function prototype
	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code 
	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

		/*case WM_PAINT:
		display();
		break;
		return(0);*/

		/*case WM_ERASEBKGND:
		return(0);*/

	case WM_SIZE:
		resize(LOWORD(lparam), HIWORD(lparam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // F
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		case VK_RIGHT:

			onRightKeyPressed();
			break;

		case VK_LEFT:
			onLeftKeyPressed();
			break;

		case VK_UP:
			onUpKeyPressed();
			break;

		case VK_DOWN:
			onDownKeyPressed();
			break;

		case VK_ADD:
			onNumKeyboardPlus();
			break;

		case VK_SUBTRACT:
			onNumKeyboardMinus();
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, imsg, wParam, lparam));
}
void ToggleFullScreen(void)
{
	//Variable Declerations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}
	else
	{
		//code 
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable decleration
	PIXELFORMATDESCRIPTOR pfd;

	int ipixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize
	pfd.nSize = sizeof(PPIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; //Changes for 3D window

	ghdc = GetDC(ghwnd);

	ipixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (ipixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, ipixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


	resize(WIN_WIDTH, WIN_HEIGHT);

	CameraMatrix[0] = 1.0f;
	CameraMatrix[1] = 0.0f;
	CameraMatrix[2] = 0.0f;
	CameraMatrix[3] = 0.0f;

	CameraMatrix[4] = 0.0f;
	CameraMatrix[5] = 1.0f;
	CameraMatrix[6] = 0.0f;
	CameraMatrix[7] = 0.0f;

	CameraMatrix[8] = 0.0f;
	CameraMatrix[9] = 0.0f;
	CameraMatrix[10] = 1.0f;
	CameraMatrix[11] = 0.0f;

	CameraMatrix[12] = 0.0f;
	CameraMatrix[13] = 0.0f;
	CameraMatrix[14] = 0.0f;
	CameraMatrix[15] = 1.0f;

	position[0] = 0.0f;
	position[1] = 0.0f;
	position[2] = 5.0f; //set position acooring to viewing distance
	
	upVector[0] = 0.0f;
	upVector[1] = 1.0f;
	upVector[2] = 0.0f;

	target[0] = 0.0f;
	target[1] = 0.0f;
	target[2] = 0.0f;
}

void display(void)
{
	//code 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt(position[0], position[1], position[2], 0, 0,0, upVector[0], upVector[1], upVector[2] );
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//quadric = gluNewQuadric();
	//glColor3f(1.0f, 1.0f, 0.0f);

	//3d param i.e. slices (like longitude) and 4th param i.e stacks (like lattitudes)
	// it is like tesselation more 3rd 4th param makes sphere smooth
	//gluSphere(quadric, 0.75, 30, 30);
    DrawCube();

	//glFlush(); -- Commented single buffer api
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, ((GLfloat)width / (GLfloat)height),0.01, 100.0);
}

void uninitialize()
{
	//uninitialize code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE |
			SWP_NOOWNERZORDER |
			SWP_NOZORDER |
			SWP_NOSIZE
			| SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);

	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
}