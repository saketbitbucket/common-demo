#include <Windows.h>
#include <gl/GL.h>

#define _USE_MATH_DEFINES
#include <math.h>

#define WIN_WIDTH  800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable decleration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

int gDkeyPressed = 0;
//Using my old function here
void DrawOneRainbowArc(GLfloat centerX, GLfloat centerY, GLfloat centerZ, GLfloat radius, 
	   GLfloat colorX, GLfloat colorY, GLfloat colorZ)
{
	glColor3f(colorX, colorY, colorZ);
	glBegin(GL_LINE_LOOP);
	{
		for (float angle = (M_PI *(3/2)); angle < (M_PI*2); angle = angle + 0.01f)
		{
			glVertex3f(centerX + radius*sin(angle), centerY + radius*cos(angle), 0.0f);
		}

	}glEnd();
}

void DrawfilledCircle(GLfloat centerX, GLfloat centerY, GLfloat centerZ, GLfloat radius)
{
	glColor3f(1.0f, 1.0f, 1.0f);

	float gap = radius / 100;
	glLineWidth(5.0f);
	
	float i = radius;
	for (float i = radius; i > 0.0f;)
	{
		glBegin(GL_LINE_LOOP);
		{
			for (float angle = 0; angle < (2 * M_PI); angle = angle + 0.01f)
			{
				glVertex3f(centerX+ i*sin(angle), centerY+i*cos(angle), 0.0f);
			}
			i = i - gap;
		}
		glEnd();
	}
}

void DrawfilledEllipse(GLfloat centerX, GLfloat centerY, GLfloat centerZ, GLfloat Hradius, GLfloat Vradius)
{
	glColor3f(1.0f, 1.0f, 1.0f);

	float Hgap = Hradius / 100;
	float Vgap = Vradius / 100;
	glLineWidth(5.0f);

	float i = Hradius;
	float j = Vradius;

	while (i > 0.0f || j > 0.0f)
	{
		glBegin(GL_LINE_LOOP);
		{
			for (float angle = 0; angle < (2 * M_PI); angle = angle + 0.01f)
			{
				glVertex3f(centerX + i*sin(angle), centerY + (j)*cos(angle), 0.0f);
			}
			i = i - Hgap;
			j = j - Vgap;
		}
		glEnd();
	}
}

void DrawCloud1()
{
	glLoadIdentity();
	glTranslatef(-0.5f, -0.9f, 0);
	DrawfilledCircle(0.0f, 0.0f, 0.0f, 0.20f);
	DrawfilledCircle(-0.40f, -0.15f, 0.0f, 0.10f);
	DrawfilledCircle(-0.20f, -0.15f, 0.0f, 0.21f);
	DrawfilledCircle(0.30f, -0.10f, 0.0f, 0.18f);
}

void DrawCloud2()
{
	glLoadIdentity();
	glTranslatef(-0.7f, 0.75f, 0);
	DrawfilledEllipse(0.0f, 0.0f, 0.0f, 0.16f, 0.10f);
	DrawfilledEllipse(0.1f, 0.0f, 0.0f, 0.18f, 0.07f);
	DrawfilledEllipse(-0.035f, 0.07f, 0, 0.08f, 0.06f);
	DrawfilledEllipse(-0.15f, 0.0f, 0.0f, 0.15f, 0.08f);
}

void DrawCloud3()
{
	glLoadIdentity();
	glTranslatef(0.92f, -0.3f, 0);
	DrawfilledEllipse(0.0f, 0.0f, 0.0f, 0.25f, 0.09f);
	DrawfilledEllipse(-0.04f, 0.04f, 0.0f, 0.10f, 0.10f);
	DrawfilledEllipse(0.05f, 0.01f, 0.0f, 0.12f, 0.10f);
	DrawfilledEllipse(-0.03f, -0.06f, 0, 0.11f, 0.07f);
}

void DrawRainBow()
{
	GLfloat colors[7][3] =
	{
		{ 1.0f,0.0f,0.0f },
		{ 1.0f,0.49f,0.0f },
		{ 1.0f,1.0f,0.0f },
		{ 0.0f,1.0f,0.0f },
		{ 0.0f,0.0f,1.0f },
		{ 0.29f,0.0f,0.50f },
		{ 0.58f,0.0f,0.82f }
	};

	float radius = 2.0f; //max arc
	float gap = 0.7 / 7.0;

	glLineWidth(5.0f);
	for (int i = 0; i < 7; i++)
	{
		float outerradius = radius - (i*gap);
		float innerradius = radius - ((i + 1)*gap);
		float j = outerradius;
		while (innerradius <= j)
		{
			DrawOneRainbowArc(1.0f, -1.0f, 0.0f, j, colors[i][0], colors[i][1], colors[i][2]);
			j = j - 0.005f;
		}
	}
}

//Main()

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//Variable decleration

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//Code
	//Initilizing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Register Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline using Native Windowing : RainbowDemo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(980-600),
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}

//Wnd Proc()

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lparam)
{
	//Function prototype
	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code 
	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

		/*case WM_PAINT:
		display();
		break;
		return(0);*/

		/*case WM_ERASEBKGND:
		return(0);*/

	case WM_SIZE:
		resize(LOWORD(lparam), HIWORD(lparam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // F
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x44:
			if (gDkeyPressed == 4)
				gDkeyPressed = 0;
			else
				gDkeyPressed++;
		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, imsg, wParam, lparam));
}
void ToggleFullScreen(void)
{
	//Variable Declerations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}
	else
	{
		//code 
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable decleration
	PIXELFORMATDESCRIPTOR pfd;

	int ipixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize
	pfd.nSize = sizeof(PPIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	ipixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (ipixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, ipixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(100.0f / 255.0f, 149.0f/ 255.0f, 237.0f / 255.0f,0.0f);
	//glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	//resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code 
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Rendering Command
	if (gDkeyPressed != 4)
	    DrawRainBow();

	if(!(0 < gDkeyPressed && gDkeyPressed<= 4))
	   DrawCloud1();

	if (!(1 < gDkeyPressed && gDkeyPressed <= 4))
	    DrawCloud2();

	if (!(2 < gDkeyPressed && gDkeyPressed <= 4))
	    DrawCloud3();


	/*glTranslatef(-0.7f, 0.6f, 0);
	DrawfilledCircle(0.0f, 0.0f, 0.0f, 0.20f);
	DrawfilledCircle(0.15f, 0.15f, 0.0f, 0.13f);
	DrawfilledCircle(-0.15f, -0.15f, 0.0f, 0.13f);

	glLoadIdentity();
	glPointSize(2.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_POINTS);
	{
		glVertex3f(0.0f, 0.0f, 0.0f);
	}glEnd();*/

	//glFlush();
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize()
{
	//uninitialize code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE |
			SWP_NOOWNERZORDER |
			SWP_NOZORDER |
			SWP_NOSIZE
			| SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);

	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
}