
//Note: Grid intersection lines are the actual coordinates

#include <Windows.h>
#include <gl/GL.h>
#include "stdlib.h"
#include <time.h>

#define _USE_MATH_DEFINES
#include "math.h"

#define WIN_WIDTH  800
#define WIN_HEIGHT 800

#define ROW 50
#define COL 50

#define MAX_SNAKE_SIZE 50
#pragma comment(lib,"opengl32.lib")

enum SnakeDirection
{
	PlusX,
	MinusX,
	PlusY,
	MinusY
};

SnakeDirection snakeDirection = SnakeDirection::PlusX; //Direction wrt to world system


GLfloat yThickness = (2.0 / ROW);
GLfloat xThickness = (2.0 / COL);

GLfloat translateLength = xThickness;

float posX = ((ROW/4)*xThickness) +(xThickness/2); //position of insect
float posY = ((ROW/8) * yThickness) -(yThickness/2); //position of insect
int count = -1; //for color change
bool dntTrans = false;
bool bReDrawInsect = false;
bool bChangecolor = false;

int loopUpdateCount = 1;

int score = 0;

//Perfrom all operation on first snake block all other block copy it -- like a train
//Here class is used to just bind data and function no OOPS programing style followed
class SnakeBlcok
{
public:
	GLfloat ModelViewMatrix[16];

	SnakeBlcok() //constructor
	{
		ModelViewMatrix[0] = 1.0f;
		ModelViewMatrix[1] = 0.0f;
		ModelViewMatrix[2] = 0.0f;
		ModelViewMatrix[3] = 0.0f;

		ModelViewMatrix[4] = 0.0f;
		ModelViewMatrix[5] = 1.0f;
		ModelViewMatrix[6] = 0.0f;
		ModelViewMatrix[7] = 0.0f;

		ModelViewMatrix[8] = 0.0f;
		ModelViewMatrix[9] = 0.0f;
		ModelViewMatrix[10] = 1.0f;
		ModelViewMatrix[11] = 0.0f;

		ModelViewMatrix[12] = 0.0f;
		ModelViewMatrix[13] = 0.0f;
		ModelViewMatrix[14] = 0.0f;
		ModelViewMatrix[15] = 1.0f;
	}
};

SnakeBlcok snake[MAX_SNAKE_SIZE];

int snakeSize = 5;

bool MultMatrix(float *matA, float *matB, int rA, int cA, int rB, int cB, float * result)
{
	if (cA != rB)
		return false; //can't muiltply

	float temp[16]; int rC = rA;  int cC = cB;

	for (int i = 0; i < rA; i++)
	{
		for (int j = 0; j < cB; j++)
		{
			float sum = 0.0;
			for (int k = 0; k < rB; k++)
				sum = sum + matA[i * cA + k] * matB[k * cB + j];
			temp[i * cC + j] = sum;
		}
	}

	int size = rC*cC;
	for (int i = 0; i < size; i++)
	{
		result[i] = temp[i];
	}

	return true;
}

bool Translate(float x, float y, float z, float* ModelViewMatrix)
{
	GLfloat TranslationMatrix[16];
	
	//This function takes translation unit and create new CTM
	TranslationMatrix[0] = 1.0f;
	TranslationMatrix[1] = 0.0f;
	TranslationMatrix[2] = 0.0f;
	TranslationMatrix[3] = 0.0f;

	TranslationMatrix[4] = 0.0f;
	TranslationMatrix[5] = 1.0f;
	TranslationMatrix[6] = 0.0f;
	TranslationMatrix[7] = 0.0f;

	TranslationMatrix[8] = 0.0f;
	TranslationMatrix[9] = 0.0f;
	TranslationMatrix[10] = 1.0f;
	TranslationMatrix[11] = 0.0f;

	TranslationMatrix[12] = x;
	TranslationMatrix[13] = y;
	TranslationMatrix[14] = z;
	TranslationMatrix[15] = 1.0f;

	MultMatrix(TranslationMatrix, ModelViewMatrix, 4, 4, 4, 4, ModelViewMatrix);

	return true;
}

bool RotateZ(float angle, float* ModelViewMatrix)
{
	GLfloat angleRadian = angle *(M_PI / 180);

	GLfloat RotateMatrixZ[16];
	//This function takes translation unit and create new CTM
	RotateMatrixZ[0] = cos(angleRadian);
	RotateMatrixZ[1] = sin(angleRadian);
	RotateMatrixZ[2] = 0.0f;
	RotateMatrixZ[3] = 0.0f;

	RotateMatrixZ[4] = -sin(angleRadian);
	RotateMatrixZ[5] = cos(angleRadian);
	RotateMatrixZ[6] = 0.0f;
	RotateMatrixZ[7] = 0.0f;

	RotateMatrixZ[8] = 0.0f;
	RotateMatrixZ[9] = 0.0f;
	RotateMatrixZ[10] = 1.0f;
	RotateMatrixZ[11] = 0.0f;

	RotateMatrixZ[12] = 0.0f;
	RotateMatrixZ[13] = 0.0f;
	RotateMatrixZ[14] = 0.0f;
	RotateMatrixZ[15] = 1.0f;

	MultMatrix(RotateMatrixZ, ModelViewMatrix, 4, 4, 4, 4, ModelViewMatrix);

	return true;
}

bool ComputeTransformedPoints(float *transformMatrix, float* points, float* transformedPoints )
{
	float transposeTransform[16];
	for(int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			*(transposeTransform+(i*4+j)) = *(transformMatrix + (j * 4 + i));
		}
	}

	MultMatrix(transposeTransform,points, 4, 4, 4, 1, points);
	return true;
}

float ComputeVectorLength(float x, float y, float z)
{
	return sqrt((x*x) + (y*y) + (z*z));
}

bool ComputeSquareCollision(float *center1, float *center2, float side1, float side2)
{
	float dist = ComputeVectorLength(center1[0] - center2[0], center1[1] - center2[1], center1[2] - center2[2]);
    
	float sideSum = ((side1 / 2) + (side2 / 2));
	if (dist <= sideSum)
	    return true;

	return false;
}

bool CopyArray(float *mat1, float *mat2, int size)
{
	for (int i = 0; i < size; i++)
	{
		*(mat2+i) = *(mat1+i);
	}
  return true;
}

bool EndGame(float* snakeMouth)
{
	if (snakeMouth[0] >= (1.0f - xThickness) || snakeMouth[0] <= (-1.0f + xThickness) 
		|| snakeMouth[1] >= (1.0f - yThickness) || snakeMouth[1] <= (-1.0f + yThickness))
		return true;

	//check for the snake intersecting it self

	return false;
}

void DrawGrid()
{
	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(1.0f);
	//Drawing Horizontal lines
	glBegin(GL_LINES);
	{
		for (int i = 0; i < ROW; i++)
		{
			glVertex3f(-1.0f, -1+(yThickness*i), 0);
			glVertex3f(1.0f, -1 + (yThickness*i), 0);
		}
	}glEnd();

	//Drawing vertical line
	glBegin(GL_LINES);
	{
		for (int i = 0; i < ROW; i++)
		{
			glVertex3f(-1 + (xThickness*i), -1.0f , 0);
			glVertex3f(-1 + (xThickness*i), 1.0f , 0);
		}
	}glEnd();

	//Avoid if else clecking draw horizonatal and vetical border with red color
	glColor3f(1.0f, 0.0f, 0.0f);
	glLineWidth(5.0f);
	glBegin(GL_LINE_LOOP);
	{
		glVertex3f(-1.0f, 1.0f, 0);
		glVertex3f(1.0f, 1.0f, 0);
		glVertex3f(1.0f, -1.0f, 0);
		glVertex3f(-1.0f, -1.0f, 0);
	}glEnd();

}

void SetSnakeColor()
{
	if (bChangecolor && count < 800)
	{
		glColor3f(0.0, 1.0, 1.0);
	}
	else
	{
		glColor3f(0.0f, 1.0f, 0.0f);
		count = -1;//reset to original
		bChangecolor = false;
	}
}

void DrawSnakeBlock()
{
	glBegin(GL_QUADS);
	{
		glVertex3f(-(xThickness / 2), (yThickness / 2), 0);
		glVertex3f((xThickness / 2), (yThickness / 2), 0);

		glVertex3f((xThickness / 2), -(yThickness / 2), 0);
		glVertex3f(-(xThickness / 2), -(yThickness / 2), 0);
	}glEnd();
}

void DrawSnake()
{
	// Snake is a combinatio od snake blocks 
	// Each block has ame size and drawn at its own origine of object space using gl quards
	// Each block has its position stored in its MV matrix
	// So go to identity then load mv matrix and then draw gl quards along mv matrix
	// color and other attributes are applied along with that
	glMatrixMode(GL_MODELVIEW);
	
	for (int i = 0; i < snakeSize; i++)
	{
		//Reset to origin the matrix
		glLoadIdentity();

		glTranslatef((xThickness / 2), -(yThickness / 2), 0); //to position snake in middle of grid
		glMultMatrixf(snake[i].ModelViewMatrix);

		SetSnakeColor();

		DrawSnakeBlock();
	}
}

void GenerateInsectPosition()
{
	posX = rand();
	posY = rand();

	// X and Y are position inside 0 to RAND_MAX map them inside the range -1 to 1

	posX /= RAND_MAX;
	posY /= RAND_MAX;

	posX = (-1+ xThickness) + posX*2;
	posY = (-1+ yThickness) + posY*2;

	// Generate point must be multiple of xThickness and yThickness
	// to place the insect in middle of the grid
	float factor = posX / xThickness;
	factor = floor(factor);
	posX = factor*xThickness;

	factor = posY / xThickness;
	factor = floor(factor);
	posY = factor*xThickness;

	// The posX and posy are the points not in the middle of grid it is on intersection line of grid
	// Move the point a bit so that it comes between grid by half of thickness of each box
	posX += (xThickness / 2);
	posY += (yThickness / 2);
}

void DrawInsect()
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	if (bReDrawInsect)
	{
		GenerateInsectPosition();
		bReDrawInsect = false;
		bChangecolor = true;
		count = 0;
		score++;
	}
	glTranslatef(posX, posY, 0);
	
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(-(xThickness / 2), (yThickness / 2), 0);
		glVertex3f((xThickness / 2), (yThickness / 2), 0);

		glVertex3f((xThickness / 2), -(yThickness / 2), 0);
		glVertex3f(-(xThickness / 2), -(yThickness / 2), 0);
	}glEnd();

	glPopMatrix();
}

//Key Press handling
// on every keydown decide wheather to rotate or not
// by default snake will translate along its own x axis (x axis of abject coordinate)
// Snakes mouth is along x axis and snake moves along direction of its mouth
void onRightKeyPressed()
{
	glMatrixMode(GL_MODELVIEW);
	if (snakeDirection == SnakeDirection::PlusY)
	{
		RotateZ(-90, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::PlusX;
	}
	
	if (snakeDirection == SnakeDirection::MinusY)
	{
		RotateZ(90.0f, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::PlusX;
	}
}

void onUpKeyPressed()
{
	if (snakeDirection == SnakeDirection::PlusX)
	{
		RotateZ(90, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::PlusY;
	}

	if (snakeDirection == SnakeDirection::MinusX)
	{
		RotateZ(-90, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::PlusY;
	}
}

void onLeftKeyPressed()
{
	if (snakeDirection == SnakeDirection::PlusY)
	{
		RotateZ(90, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::MinusX;
	}

	if (snakeDirection == SnakeDirection::MinusY)
	{
		RotateZ(-90, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::MinusX;
	}
}

void onDownKeyPressed()
{
	if (snakeDirection == SnakeDirection::PlusX)
	{
		RotateZ(-90, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::MinusY;
	}

	if (snakeDirection == SnakeDirection::MinusX)
	{
		RotateZ(90, snake[0].ModelViewMatrix);
		snakeDirection = SnakeDirection::MinusY;
	}	
}

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable decleration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

//Main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//Variable decleration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;
	//Code

	//Initilizing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;



	//Register Class
	RegisterClassEx(&wndclass);



	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Snake Game using gl transform"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		125,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();
	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				 display();
			}
		}
	}

	uninitialize();
	return ((int)msg.wParam);

}
//Wnd Proc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lparam)
{
	//Function prototype
	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);


	//code 
	switch (imsg)
	{
	case WM_ACTIVATE:

		if (HIWORD(wParam) == 0)

			gbActiveWindow = true;

		else
			gbActiveWindow = false;
		break;

		/*case WM_PAINT:

		display();

		break;

		return(0);

		case WM_ERASEBKGND:

		return(0);*/

	case WM_SIZE:
		resize(LOWORD(lparam), HIWORD(lparam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // F
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		case VK_RIGHT:
			
			onRightKeyPressed();
			break;

		case VK_LEFT:
			onLeftKeyPressed();
			break;

		case VK_UP:
			onUpKeyPressed();
			break;

		case VK_DOWN:
			onDownKeyPressed();
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, imsg, wParam, lparam));

}

void ToggleFullScreen(void)
{
	//Variable Declerations
	MONITORINFO mi;
	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}
	else
	{
		//code 
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable decleration
	PIXELFORMATDESCRIPTOR pfd;

	int ipixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize
	pfd.nSize = sizeof(PPIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	ipixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (ipixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, ipixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	

	//Initialize the snake i.e position each block
	for (int i = 0; i < snakeSize; i++)
	{
		Translate(-xThickness*i,0,0,snake[i].ModelViewMatrix);
	}
}

void display(void)
{	//code 
	
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	if (loopUpdateCount % 250 == 0)
	{
		//Get the position of snake mouth
		float SnakeCenter[4] = { (xThickness / 2), -(yThickness / 2), 0.0f,1.0f }; //point at the center box of grid
		ComputeTransformedPoints(snake[0].ModelViewMatrix, SnakeCenter, SnakeCenter);

		bool endGanme = EndGame(SnakeCenter);
		if (endGanme)
		{
			TCHAR buffer[MAX_PATH];
			wsprintf(buffer, TEXT("Your score is : %d"), score);
			if (MessageBox(ghwnd, buffer, TEXT("Game Over"), MB_OK))
				gbEscapeKeyIsPressed = true;
		}

		float InsectCenter[3] = { 0,0,0 };
		InsectCenter[0] = posX;
		InsectCenter[1] = posY;

		bReDrawInsect = ComputeSquareCollision(SnakeCenter, InsectCenter, xThickness, xThickness);

		if(bReDrawInsect)
			snakeSize++;
		// Copy mv matrix of each block to its previous in view 
		// Note index 0 is first block and size-1 is last block
		for (int i = snakeSize - 1; i > 0; i--)
		{
			CopyArray(snake[i - 1].ModelViewMatrix, snake[i].ModelViewMatrix, 16);
		}

		Translate(translateLength, 0, 0, snake[0].ModelViewMatrix);
		loopUpdateCount = 1;
	}

	DrawGrid();

	DrawInsect();

	DrawSnake();

	if(count >= 0)
	   count++;

	//glFlush(); -- Commented single buffer api
	SwapBuffers(ghdc);

	loopUpdateCount++;
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize()
{
	//uninitialize code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,

			SWP_NOMOVE |
			SWP_NOOWNERZORDER |
			SWP_NOZORDER |
			SWP_NOSIZE
			| SWP_FRAMECHANGED);

		ShowCursor(true);
	}
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
}